## Live updates on the Spring Boot app running in K8s ##

Let's take again previous [Kubernetes ConfigMap demo](https://bitbucket.org/tomask79/kubernetes-boot-configmap/src/master/)
as a starter and this time we're going to take a look at howto update running K8s deployment without
a downtime.

### Prerequisities ###

Again we're going to have a docker image of the simple Spring Boot MVC application deployed
in the K8s minikube cluster.

```java
    @RestController
    public class ControllerMVC {

        @Value("${my.system.property:defaultValue}")
        protected String fromSystem;

        @RequestMapping("/sayhello")
        public String mvcTest() {
            return "I'm saying hello to Kubernetes with system property "+fromSystem+" !";
        }
    }
```
So again please:

* Start the docker at your environment
* Start the K8s cluster or minikube in my case
* Switch your docker daemon to the minikube: eval $(minikube docker-env) 
* mvn clean install the project to build the docker image.
* Upload the deployment.yaml and service.yaml manifests attached in this repo to your K8s cluster...
Check [first repo](https://bitbucket.org/tomask79/spring-boot-kubernetes-deploy/src/master/) for more details. 

Desired state is:

    tomask79:kubernetes-update tomask79$ kubectl get pods
    NAME                             READY     STATUS    RESTARTS   AGE
    hello-minikube-6c47c66d8-gzvgc   1/1       Running   6          29d
    test-app-58b899455b-h57ss        1/1       Running   0          23h
    tomask79:kubernetes-update tomask79$ kubectl get deployment
    NAME             DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
    hello-minikube   1         1         1            1           29d
    test-app         1         1         1            1           3d
    tomask79:kubernetes-update tomask79$ kubectl get services
    NAME         TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)          AGE
    kubernetes   ClusterIP   10.96.0.1      <none>        443/TCP          29d
    test-app     NodePort    10.105.171.8   <none>        8081:30615/TCP   3d
    tomask79:kubernetes-update tomask79$ 

## Option 1: Manual editing of the running deployment ##

First option of howto update a running deployment is through: [kubectl edit](https://kubernetes-v1-4.github.io/docs/user-guide/kubectl/kubectl_edit/)
To test it let's prepare another **configmap with name option1 and key/value my.system.property = forEdit**

    tomask79:kubernetes-update tomask79$ kubectl create configmap option1 --from-literal=my.system.property=forEdit
    configmap "option1" created

Now discover where service is running and curl it:

    tomask79:kubernetes-update tomask79$ minikube service test-app --url
    http://192.168.99.100:30615

    tomask79:kubernetes-update tomask79$ curl http://192.168.99.100:30615/sayhello
    I'm saying hello to Kubernetes with system property changeValue2222 !

Now let's test **kubectl edit** and perform the **switch to our new configmap option1** In the vim editor
find the configmap part...
    
    spec:
          containers:
          - env:
            - name: my.system.property
              valueFrom:
                configMapKeyRef:
                  key: my.system.property
                  name: <configmapname>

and change the <configmapname> to option1. After the change, !wq the vim editor.
 
    tomask79:kubernetes-update tomask79$ kubectl edit deployments/test-app
    deployment.extensions "test-app" edited

K8s will update the running deployment. Curl service again...

    tomask79:kubernetes-update tomask79$ curl http://192.168.99.100:30615/sayhello
    I'm saying hello to Kubernetes with system property forEdit !

Deployment updated! Kubectl edit certainly looks good as option for admins. 
But this lacks some sort of automation.

## Option 2: Patching the running deployment ##

Another option of howto update running deployment is through: [kubectl patch](https://kubernetes-v1-4.github.io/docs/user-guide/kubectl/kubectl_patch/)
Again let's prepare another configmap with name option2 and key/value my.system.property=forPatch

    tomask79:kubernetes-update tomask79$ kubectl create configmap option2 --from-literal=my.system.property=forPatch
    configmap "option2" created

Now let's prepare something interesting, patch file **test-app-patch.yaml** to update the deployment:

    spec:
      template:
        spec:
          containers:
          - name: test-app
            image: test-controller:1.0-SNAPSHOT
            env:
              # Define the environment variable
              - name: my.system.property
                valueFrom:
                  configMapKeyRef:
                    # The ConfigMap containing the value you want to assign to my.system.property
                    name: option2
                    # Specify the key associated with the value
                    key: my.system.property

and use this file to patch the deployment **without an outage**:

    tomask79:kubernetes-update tomask79$ kubectl patch deployment test-app --type merge --patch "$(cat test-app-patch.yaml)"
    deployment.extensions "test-app" patched

Okay, let's curl the service:

    tomask79:kubernetes-update tomask79$ curl http://192.168.99.100:30615/sayhello
    I'm saying hello to Kubernetes with system property forPatch !

Deployment updated! Personally I like this approach, because we didn't change the first initial configuration.
And patch files can be versioned in GIT for example...Anyway I also definitelly recommend to check [kubectl replace](https://kubernetes-v1-4.github.io/docs/user-guide/kubectl/kubectl_replace/)
and [kubectl apply](https://kubernetes-v1-4.github.io/docs/user-guide/kubectl/kubectl_apply/) for editing the K8s
resources. 

Best regards

Tomas