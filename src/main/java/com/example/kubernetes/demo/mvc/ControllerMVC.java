package com.example.kubernetes.demo.mvc;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by tomask79 on 20.06.18.
 */
@RestController
public class ControllerMVC {

    @Value("${my.system.property:defaultValue}")
    protected String fromSystem;

    @RequestMapping("/sayhello")
    public String mvcTest() {
        return "I'm saying hello to Kubernetes with system property "+fromSystem+" !";
    }
}
